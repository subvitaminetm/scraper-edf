import { scrap } from './scraps/scrap';
import express from 'express';
import dotenv from 'dotenv';
import bodyParser from 'body-parser';
import { validate } from 'class-validator';
// import { scrapQueueAdd } from './queues/scrap.queue';
import { ScrapDataDto } from './dto/scrap-data.dto';

dotenv.config();

const app = express();

app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));

const PORT = process.env.PORT || 4000;
const VERSION = process.env.VERSION || '1.0.0';

app.get('/', (_, res) => {
  res.json({
    name: 'Microservice Scrap Boilerplate',
    version: VERSION,
  });
});

app.post('/webhook', async (req, res) => {
  console.log(req.body);
  res.json({
    message: 'ok',
  });
});
app.post('/scrap', async (req, res) => {
  const data = Object.assign(new ScrapDataDto(), req.body);

  const errors = await validate(data);
  if (errors.length) {
    res.status(400);
    res.json({
      errors,
    });
    return;
  }
  scrap(data);
  res.json({
    message: 'ok',
  });
  return;
});

app.listen(PORT, () => {
  console.log(`listen on PORT ${PORT}`);
});
