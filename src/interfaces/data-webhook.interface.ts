import { Facture } from './facture.interface';

export interface IDataWebhook {
  dirName: string;
  fileName: string;
  facture: Facture | undefined;
}
