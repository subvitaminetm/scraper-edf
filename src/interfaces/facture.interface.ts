export interface TaxesFacture {
  montantTVA: number;
}

export interface MontantFacture {
  totalAPayer: number;
  montantTTC: number;
  montantHT: number;
}

export interface IdentiteFacture {
  identifiantTechnique: string;
  identifiant: string;
}

export interface CaracteristiquesFacture {
  typeFacture: string;
  dateLegaleFacture: string;
  dateEcheanceFacture: string;
}

export interface CaracteristiquesEditiqueFacture {
  telephoneAccueilEDF: string;
  libelleModeleFacture: string;
  codeModeleFacture: string;
}

export interface Facture {
  taxesFacture: TaxesFacture;
  montantFacture: MontantFacture;
  identiteFacture: IdentiteFacture;
  caracteristiquesFacture: CaracteristiquesFacture;
  caracteristiquesEditiqueFacture: CaracteristiquesEditiqueFacture;
}

export interface ListFatures {
  total: number;
  factures: Facture[];
}
