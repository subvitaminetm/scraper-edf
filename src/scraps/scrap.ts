import { webhookQueueAdd } from './../queues/webhook.queue';
import puppeteer, { LaunchOptions } from 'puppeteer';
import fs from 'fs-extra';
import path from 'path';
import { IDataScrap } from './../interfaces/data-scrap.interface';
import { sleep } from '../utils/sleep';
import dayjs from 'dayjs';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import { ListFatures } from 'src/interfaces/facture.interface';
dayjs.extend(customParseFormat);
const url = 'https://www.edfentreprises.fr/';

const regexPdf = RegExp('(getFacturePdfLink=1)$');
const regexList = RegExp('(getFacturesbyId=1)$');

const launchOptions: LaunchOptions = {
  headless: false,
  product: 'chrome',
  args: [
    // Required for Docker version of Puppeteer
    '--no-sandbox',
    '--disable-setuid-sandbox',
    // This will write shared memory files into /tmp instead of /dev/shm,
    // because Docker’s default for /dev/shm is 64MB
    '--disable-dev-shm-usage',
  ],
};

export const scrap = async ({
  login,
  password,
  ...data
}: IDataScrap): Promise<void> => {
  const browser = await puppeteer.launch(launchOptions);

  try {
    const page = await browser.newPage();
    await page.goto(url);
    await page.waitForSelector('input#idToken1');
    await page.type('input#idToken1', login);
    await page.type('input#idToken2', password);
    await page.keyboard.press('Enter');
    await page.waitForNavigation();
    await page.goto(
      'https://entreprises-collectivites.edf.fr/espaceclient/s/factures',
    );
    await page.waitForSelector(
      'button[class="slds-button slds-section__title-action"]',
      { visible: true },
    );
    const tabBtn = await page.evaluate(() => {
      const buttons = document.querySelectorAll(
        'button[class="slds-button slds-section__title-action"]',
      );
      const ids: string[] = [];
      buttons.forEach((item) => {
        ids.push(item.id);
      });
      return ids;
    });
    for (const tab of tabBtn) {
      await page.click('#' + tab);
      const resp = await page.waitForResponse((res) =>
        regexList.test(res.url()),
      );
      const json = (await resp.json()) as any;
      if (!json.actions[0].returnValue) {
        continue;
      }
      const list: ListFatures = JSON.parse(json.actions[0].returnValue);

      const ids: string[] = list.factures
        .filter((f) => {
          const date = f.caracteristiquesFacture.dateLegaleFacture;
          return dayjs(data.date, 'DD/MM/YYYY').isBefore(
            dayjs(date, 'DD/MM/YYYY'),
          );
        })
        .map((f) => f.identiteFacture.identifiant);

      const hasNext = true;
      while (hasNext) {
        const buttons = await page.$$('button[title="Télécharger"]');
        for (const button of buttons) {
          const id = await button.evaluate((el) => el.id);
          if (!ids.includes(id)) {
            continue;
          }

          const dirName = path.join(__dirname, '..', 'tmp', id);
          // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
          // @ts-ignore
          await page._client.send('Page.setDownloadBehavior', {
            behavior: 'allow',
            downloadPath: dirName,
          });
          await fs.ensureDir(dirName);
          await page.click(`button[id="${id}"]`);
          await page.waitForResponse((res) => regexPdf.test(res.url()));
          let fileName;
          do {
            await sleep(100);
            [fileName] = await fs.readdir(dirName);
          } while (!fileName || fileName.endsWith('.crdownload'));
          webhookQueueAdd({
            dirName,
            fileName,
            facture: list.factures.find(
              (f) => f.identiteFacture.identifiant === id,
            ),
          });
        }
        const nextButton = await page.$('#tableId_next');
        const hasNext = await nextButton?.evaluate(
          (el) => !el.classList.contains('disabled'),
        );
        if (!hasNext) {
          break;
        }
        if (hasNext) {
          await nextButton?.click();
        }
      }
    }
    return browser.close();
  } catch (e) {
    throw e;
  }
};
