import { IsNotEmpty, IsString } from 'class-validator';

export class ScrapDataDto {
  @IsString()
  @IsNotEmpty()
  login: string;

  @IsString()
  @IsNotEmpty()
  password: string;

  @IsString()
  date: string;
}
