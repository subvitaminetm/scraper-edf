import { scrap } from './../scraps/scrap';
import { IDataScrap } from './../interfaces/data-scrap.interface';
import Queue, { QueueOptions, JobOptions } from 'bull';
import dotenv from 'dotenv';

dotenv.config();

const QUEUE_NAME = 'scrap_queue';

const QUEUE_OPTIONS: QueueOptions = {
  redis: {
    host: process.env.REDIS_HOST || 'localhost',
    port: parseInt(process.env.REDIS_PORT as string) || 6379,
  },
};

const JOB_OPTIONS: JobOptions = {
  attempts: 3,
  backoff: {
    type: 'fixed',
    delay: 2000,
  },
};

export const scrapQueue = new Queue<IDataScrap>(QUEUE_NAME, QUEUE_OPTIONS);

scrapQueue.process(({ data }) => {
  return scrap(data);
});

export const scrapQueueAdd = (data: IDataScrap) =>
  scrapQueue.add(data, JOB_OPTIONS);
