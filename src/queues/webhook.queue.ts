import { IDataWebhook } from './../interfaces/data-webhook.interface';
import Queue, { QueueOptions, JobOptions } from 'bull';
import dotenv from 'dotenv';
import Axios from 'axios';
import fs from 'fs-extra';
import path from 'path';

dotenv.config();

const QUEUE_NAME = 'webhook_queue';

const QUEUE_OPTIONS: QueueOptions = {
  redis: {
    host: process.env.REDIS_HOST || 'localhost',
    port: parseInt(process.env.REDIS_PORT as string) || 6379,
  },
};

const JOB_OPTIONS: JobOptions = {
  attempts: 3,
  backoff: {
    type: 'fixed',
    delay: 2000,
  },
};

export const scrapQueue = new Queue<IDataWebhook>(QUEUE_NAME, QUEUE_OPTIONS);

scrapQueue.process(async ({ data }) => {
  const pdf = await fs.readFile(path.join(data.dirName, data.fileName));
  await fs.remove(data.dirName);
  try {
    const response = await Axios.post('http://localhost/webhook', {
      file: pdf.toString('base64'),
    });
    console.log(response.data);
    await fs.remove(data.dirName);
  } catch (e) {
    throw e;
  }
});

export const webhookQueueAdd = (data: IDataWebhook) =>
  scrapQueue.add(data, JOB_OPTIONS);
