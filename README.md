# Edf Microservice mono compte

Microservice de scrapping pour le site edf ( mono compte )

## Pour commencer

Créer le .env depuis le env-example :

```
cp env-example .env
```
Remplir le .env

```
docker-compose up -d
```

### Dévelopment

Pour le dévelopment avoir node et yarn installer
Lancer redis avec:
```
docker-compose up -d redis
```

et lancer le programme avec:
```
yarn dev
```


### Boilerplate

```bash
├── Dockerfile
├── README.md
├── docker-compose.yml
├── env-example
├── package.json
├── src
│   ├── dto
│   │   └── scrap-data.dto.ts
│   ├── index.ts
│   ├── interfaces
│   │   └── data-scrap.interface.ts
│   ├── queues
│   │   └── scrap.queue.ts
│   ├── scraps
│   │   └── scrap.ts
│   └── utils
│       └── sleep.ts
├── tsconfig.json
└── yarn.lock
```
#### Dépendances

- [express](https://github.com/expressjs/express)

- [puppeteer](https://github.com/puppeteer/puppeteer)

- [bull](https://github.com/OptimalBits/bull)

- [class-validator](https://github.com/typestack/class-validator)
